options(warn=-1)
#Process MEA files. Input should be txt file created by MC_Data tools from Burst Marker data
library(matrixStats)
#set input file with the specified title
args = commandArgs(trailingOnly=TRUE)
if (length(args)!= 1) {
    stop("You have specified the wrong number of input files!", call.=FALSE)
}
input = args[1]

####################
#### READING IN ####
####################

#read in data, remove first line and create an index vector for electrodes
dat <- as.matrix(read.table(input,fill=TRUE,row.names=NULL))
electrodes <- which(dat[,3] == " 1")
number_of_electrodes = length(electrodes)

#create one timestamp and one length object per electrode (very messy but works...)
electrode_names <- character()
for(i in 1:(number_of_electrodes - 1)){
    nam1 <- paste("timestamps", dat[electrodes[i],4], sep = "")
    nam2 <- paste("lengths", dat[electrodes[i],4], sep = "")
    val1 = 0
    val2 = 0
    if((electrodes[i] + 2) < (electrodes[i+1] - 1)){
        val1 = as.numeric(dat[(electrodes[i] + 3):(electrodes[i+1] - 1),1])/100
        val2 = as.numeric(dat[(electrodes[i] + 3):(electrodes[i+1] - 1),2])/100
    }
    assign(nam1,val1)
    assign(nam2,val2)
    electrode_names[i] <- dat[electrodes[i],4]
    
}

nam1 <- paste("timestamps", dat[electrodes[number_of_electrodes],4], sep = "")
nam2 <- paste("lengths", dat[electrodes[number_of_electrodes],4], sep = "")
val1 = 0
val2 = 0
if((electrodes[number_of_electrodes] + 2) < length(dat[,1])){
    val1 = as.numeric(dat[(electrodes[length(electrodes)] + 3):length(dat[,1]),1])/100
    val2 = as.numeric(dat[(electrodes[length(electrodes)] + 3):length(dat[,1]),2])/100
}
assign(nam1,val1)
assign(nam2,val2)
electrode_names[number_of_electrodes] <- dat[electrodes[number_of_electrodes],4]
electrode_names <- as.numeric(electrode_names)

#############################
#### PROCESSING/ANALYSIS ####
#############################

#electrode statistics: reject outliers based on size
sizes <- c()
zeros <- c()
counter = 1
for(i in electrode_names){
    timestamps <- paste("timestamps",i,sep="")
    lengths <- paste("lengths",i,sep="")
    size <- length(get(timestamps))
    if((size == 1) && (get(timestamps)[1] == 0)){
        zeros <- c(zeros,counter)
    }
    sizes <- c(sizes,size)
    counter = counter + 1
}
too_large <- which(sizes[] > 1.5*IQR(sizes) + quantile(sizes,0.75,names=FALSE))
too_small <- which(sizes[] < -1.5*IQR(sizes) + quantile(sizes,0.25,names=FALSE))
outliers <- c(too_large,zeros)
if(length(too_small) > 0){
    outliers <- c(outliers,too_small)
}
working_electrodes <- electrode_names
if(length(outliers) > 0){
    working_electrodes <- working_electrodes[-outliers]
}

#find min/max times and lengths for working electrodes and round
max_time = 0
max_length = 0
for(i in working_electrodes){
    timestamps <- paste("timestamps",i,sep="")
    timestamps_val <- get(timestamps)
    lengths <- paste("lengths",i,sep="")
    timestamps_val <- get(timestamps)
    lengths_val <- get(lengths)
    if(max(timestamps_val) > max_time){
        max_time = max(timestamps_val)
    }
    if(max(lengths_val) > max_length){
        max_length = max(lengths_val)
    }
}
min_time = max_time
for(i in working_electrodes){
    timestamps <- paste("timestamps",i,sep="")
    lengths <- paste("lengths",i,sep="")
    timestamps_val <- get(timestamps)
    lengths_val <- get(lengths)
    if((min(timestamps_val) < min_time) && (min(timestamps_val) > 0)){
        min_time = min(timestamps_val)
    }
    assign(timestamps,round(timestamps_val,digits = 0))
    assign(lengths,round(lengths_val,digits = 0))
}

#create binaries for working electrodes
time = 1:(max_time + max_length)
for(i in working_electrodes){
    timestamps <- paste("timestamps",i,sep="")
    lengths <- paste("lengths",i,sep="")
    timestamps_val <- get(timestamps)
    lengths_val <- get(lengths)
    nam <- paste("binaries",i, sep = "")
    val <- array(0,max_time + max_length)
    for(s in 1:length(timestamps_val)){
        val[(timestamps_val[s]:(timestamps_val[s] + lengths_val[s] - 1))] = 1
    }
    val <- as.logical(val)
    assign(nam,val)
}

#create bursting profile
binary_matrix <- c()
for(i in working_electrodes){
    binaries <- paste("binaries",i, sep = "")
    binaries_val <- get(binaries)
    binary_matrix <- cbind(binary_matrix,binaries_val)
}
burst_profile <- array(0,length(time))
for(t in 1:length(time)){
    burst_profile[t] = sum(binary_matrix[t,])
}

#transform bursting profile to logical vector given threshold K
K = 3
cat("Number of bursts with K = ")
cat(K)
cat("\n")

logical_burst_profile = array(0,length(burst_profile))
for(i in 1:length(burst_profile)){
    if(burst_profile[i] > K){
        logical_burst_profile[i] = 1
    }
}
logical_burst_profile <- as.logical(logical_burst_profile)


y <- rle(logical_burst_profile)
number_of_bursts <- sum(y$values)
if(number_of_bursts>100){
    number_of_bursts = 100
}
cat(number_of_bursts)
cat("\n")

#plot bursting profile
t=(1:length(burst_profile))/10
filename <- paste(input,"pdf", sep=".")
pdf(file = filename)
plot(t,burst_profile,type="l",ylim=c(5,45),xlim=c(0,600),xlab="time (s)", ylab="number of bursting electrodes",bty="l")
garbage <- dev.off()


