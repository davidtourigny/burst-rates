bold=$(tput bold)
normal=$(tput sgr0)
for dir in ./*/
do
printf ${bold}$dir
    printf ${normal}"\n"
    for i in $dir/*.txt
    do
        printf $i
        printf "\n"
        Rscript burst_rates.r $i
    done
    printf "\n"
done
